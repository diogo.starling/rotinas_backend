from flask import Flask, jsonify, request
from FlaskApi import app
from faker import Faker
from unidecode import unidecode
from datetime import datetime, timedelta
import random
import json
import re
 
def limpa_nome(name):
    # Remove os acentos, passa para minuscula, remove os espaços e caracteres especiais, mantendo apenas letras e números
    name = unidecode(name)
    name = name.lower()

    cleaned_name = re.sub(r'[^A-Za-z0-9]', '', name)
    return cleaned_name
 
#Gerar lista de nomes aleatórios
@app.route('/api/nomes_aleatorios')
def get_nomes_aleatorios():
    quantidade = request.args.get('quantidade')
    quantidade = int(quantidade)
    fake = Faker(['pt_BR'])  # Defina o idioma desejado para os nomes
    names = []
    for _ in range(quantidade):
        is_male = random.choice([True, False])
        first_name = fake.first_name_male() if is_male else fake.first_name_female()
        middle_name = fake.last_name() if is_male else fake.last_name()
        last_name = fake.last_name()
        gender = "Masculino" if is_male else "Feminino"
       
        full_name = f"{first_name} {middle_name} {last_name}"
        names.append((full_name))
    lista_nomes = list(names)
    return jsonify(lista_nomes)

#Gerar nome aleatório
@app.route('/api/nome_aleatorio')
def get_nome_aleatorio():
    fake = Faker(['pt_BR'])  # Defina o idioma desejado para os nomes
    is_male = random.choice([True, False])
    first_name = fake.first_name_male() if is_male else fake.first_name_female()
    middle_name = fake.last_name() if is_male else fake.last_name()
    last_name = fake.last_name()
    gender = "Masculino" if is_male else "Feminino"
    
    full_name = f"{first_name} {middle_name} {last_name}"
    return full_name

#Gerar nome aleatório e email
@app.route('/api/nome_email')
def get_nome_email():
    fake = Faker(['pt_BR'])  # Defina o idioma desejado para os nomes
    is_male = random.choice([True, False])
    first_name = fake.first_name_male() if is_male else fake.first_name_female()
    middle_name = fake.last_name() if is_male else fake.last_name()
    last_name = fake.last_name()
    #gender = "Masculino" if is_male else "Feminino"
    
    full_name = f"{first_name} {middle_name} {last_name}"
    first_limpo = limpa_nome(first_name)
    last_limpo = limpa_nome(last_name)
    
    email = f"{first_limpo}.{last_limpo}@caixa.teste.com.br"
    return jsonify(full_name, email)

#Gerar email aleatório
@app.route('/api/email')
def get_email():
    fake = Faker(['pt_BR'])  # Defina o idioma desejado para os nomes
    is_male = random.choice([True, False])
    first_name = fake.first_name_male() if is_male else fake.first_name_female()
    last_name = fake.last_name()
    
    first_limpo = limpa_nome(first_name)
    last_limpo = limpa_nome(last_name)
    email = f"{first_limpo}.{last_limpo}@caixa.teste.com.br"
    return  email

#Gerar nome, mae e pai aleatório
@app.route('/api/nome_mae_pai')
def get_nome_familia():
    fake = Faker(['pt_BR'])  # Defina o idioma desejado para os nomes
    is_male = random.choice([True, False])
    first_name = fake.first_name_male() if is_male else fake.first_name_female()
    middle_name = fake.last_name() if is_male else fake.last_name()
    last_name = fake.last_name()
    gender = "Masculino" if is_male else "Feminino"
    mae_name = fake.first_name_female()
    pai_name = fake.first_name_male()
    
    name = f"{first_name} {middle_name} {last_name}"
    mae = f"{mae_name} {middle_name}"
    pai = f"{pai_name} {last_name}"

    return jsonify(name, mae, pai)

#Gerador de CPF
@app.route('/api/gerar_cpf')
def gerar_cpf():
    cpf = [random.randint(0, 9) for _ in range(9)]
    # Primeiro dígito verificador
    for _ in range(2):                                                          
        val = sum([(len(cpf) + 1 - i) * v for i, v in enumerate(cpf)]) % 11      
        cpf.append(11 - val if val > 1 else 0)
    # Convertendo os números em string 
    return '%s%s%s%s%s%s%s%s%s%s%s' % tuple(cpf)
 
 #Gerador de CPF
@app.route('/api/gerar_cnpj')
def gerar_cnpj():
    # Gera os primeiros 12 dígitos do CNPJ aleatoriamente
    cnpj = [random.randint(0, 9) for _ in range(8)] + [0, 0, 0, 1]
    # Função para calcular os dígitos verificadores
    def calculate_digit(cnpj, weights):
        soma = sum(x * y for x, y in zip(cnpj, weights))
        resto = soma % 11
        return 0 if resto < 2 else 11 - resto
 
    # Pesos para o primeiro dígito verificador
    weights_first_digit = [5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2]
    first_digit = calculate_digit(cnpj, weights_first_digit)
    cnpj.append(first_digit)
 
    # Pesos para o segundo dígito verificador
    weights_second_digit = [6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2]
    second_digit = calculate_digit(cnpj, weights_second_digit)
    cnpj.append(second_digit)
    # Convertendo os números em string e formatando o CNPJ
    cnpj_str = ''.join(map(str, cnpj))
   # cnpj_formatado = f"{cnpj_str[:2]}.{cnpj_str[2:5]}.{cnpj_str[5:8]}/{cnpj_str[8:12]}-{cnpj_str[12:]}"
    return cnpj_str

#Gerador data de nascimento adulto
@app.route('/api/gerar_dt_nasc_adulto')
def gerar_data_nascimento():
    # Define um intervalo de idade (por exemplo, entre 18 e 70 anos)
    idade = random.randint(18, 59)
    data_atual = datetime.now()
    data_nascimento = data_atual - timedelta(days=350 * idade)
    return  data_nascimento.strftime("%Y-%m-%d %H:%M:%S")

#Gerador data de nascimento idoso
@app.route('/api/gerar_dt_nasc_idoso')
def gerar_data_nasc_idoso():
    # Define um intervalo de idade (por exemplo, entre 65 e 90 anos)
    idade = random.randint(60, 100)
    data_atual = datetime.now()
    data_nascimento = data_atual - timedelta(days=350 * idade)
    return  data_nascimento.strftime("%Y-%m-%d %H:%M:%S")

#Gerador data de nascimento criança 
@app.route('/api/gerar_dt_nasc_crianca')
def gerar_data_nasc_crianca():
    # Define um intervalo de idade (por exemplo, entre 0 e 12 anos)
    idade = random.randint(0, 12)
    data_atual = datetime.now()
    data_nascimento = data_atual - timedelta(days=350 * idade)
    return  data_nascimento.strftime("%Y-%m-%d %H:%M:%S")

#Gerador data de nascimento adolescente maior que 12 anos
@app.route('/api/gerar_dt_nasc_adolescente')
def gerar_data_nasc_adolescente():
    # Define um intervalo de idade (por exemplo, entre 13 e 18 anos)
    idade = random.randint(13, 17)
    data_atual = datetime.now()
    data_nascimento = data_atual - timedelta(days=350 * idade)
    return data_nascimento.strftime("%Y-%m-%d %H:%M:%S")

#Gerador de data atual
@app.route('/api/data_atual')
def gerar_data_atual():
    data_atual = datetime.now()
    return data_atual.strftime("%Y-%m-%d %H:%M:%S")

#Gerador de data mês anterior
@app.route('/api/data_mes_anterior')
def gerar_data_mes_anterior():
    data_atual = datetime.now()
    data_anterior = data_atual - timedelta(days=30)
    return data_anterior.strftime("%Y-%m-%d %H:%M:%S")
   
#Gerador de data ano anterior
@app.route('/api/data_ano_anterior')
def gerar_data_ano_anterior():
    data_atual = datetime.now()
    ano_anterior = data_atual - timedelta(days=365)
    return ano_anterior.strftime("%Y-%m-%d %H:%M:%S")

#Gerador de data, hora e milisegundo atual
@app.route('/api/gerar_datetime')
def gerar_datetime():
    data_time = datetime.now()
    return data_time.strftime("%Y-%m-%d %H:%M:%S.%f")[:-3]

#Gerador de data e hora atual
@app.route('/api/gerar_date')
def gerar_date():
    data_time = datetime.now()
    return data_time.strftime("%Y-%m-%d %H:%M:%S")

#Gerador de contrato
@app.route('/api/gerar_contrato')
def gerar_contrato():
    contrato = ''.join(random.choices('0123456789', k=20))
    return contrato

#Gera um número de conta de 9 dígitos
@app.route('/api/conta')
def gera_conta():
    conta = ''.join(random.choices('0123456789', k=9))
    return conta

#Calcula o digito da conta  
@app.route('/api/calculaDvConta')
def calcula_dv():
    conta =  request.args.get('conta')
    # Converte a string de números em uma lista de inteiros
    digits = [int(digit) for digit in conta]
    # Define os pesos de multiplicação (começando de trás para frente, de 2 a 11)
    weights = list(range(2, 12)) * ((len(digits) + 10) // 11)
    weights = weights[:len(digits)]
    # Calcula a soma ponderada dos dígitos
    total = sum(d * w for d, w in zip(reversed(digits), weights))
    # Calcula o dígito verificador usando o módulo 11
    remainder = total % 11
    dv = 11 - remainder
    if dv >= 10:
        dv = 0
    return str(dv)

def gerar_cpf_asd():
    cpf1 = [random.randint(0, 9) for _ in range(9)]
    # Primeiro dígito verificador
    soma = sum((len(cpf) + 1 - i) * v for i, v in enumerate(cpf))
    resto = 11 - (soma % 11)
    if resto >= 10:
        resto = 0
    cpf1.append(resto)
    # Segundo dígito verificador
    soma = sum((len(cpf) - i) * v for i, v in enumerate(cpf))
    resto = 11 - (soma % 11)
    if resto >= 10:
        resto = 0
    cpf1.append(resto)
    # Convertendo os números em string e formatando o CPF
    cpf_str = ''.join([str(digit) for digit in cpf1])
    cpf_formatado = f"{cpf_str[:3]}.{cpf_str[3:6]}.{cpf_str[6:9]}-{cpf_str[9:]}"
    return cpf_str