import os
from FlaskApi import app    # Imports the code from HelloFlask/__init__.py

if __name__ == '__main__':
    HOST = os.environ.get('SERVER_HOST', 'localhost')

    try:
        PORT = int(os.environ.get('SERVER_PORT', '5555'))
    except ValueError:
        PORT = 5555
    try:
        app.json.ensure_ascii = False
    except:
        app.config['JSON_As_ASCII'] = False

    app.json.ensure_ascii = False
    app.run(host='localhost', port=5555)